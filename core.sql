PRAGMA foreign_keys=ON;
BEGIN TRANSACTION;

DROP TABLE IF EXISTS [content];
CREATE TABLE [content] (
  [_id]         TEXT UNIQUE PRIMARY KEY,
  [_user_id]    TEXT UNIQUE,
  [_status_id]  INTEGER NOT NULL DEFAULT 0,
  [_type_id]    INTEGER NOT NULL DEFAULT 0,
  [title]       VARCHAR(64),
  [body]        TEXT,
  [created]     DATETIME NOT NULL DEFAULT (datetime('now','localtime')),
  [updated]     DATETIME NOT NULL DEFAULT (datetime('now','localtime'))
);

DROP TABLE IF EXISTS [statuses];
CREATE TABLE [statuses] (
  [_id]         TEXT UNIQUE PRIMARY KEY,
  [name] varchar(128) NOT NULL DEFAULT '',
  [spec] varchar(128) NOT NULL DEFAULT ''
);

DROP TABLE IF EXISTS [sessions];
CREATE TABLE [sessions] (
  [_id]         TEXT UNIQUE PRIMARY KEY,
  [user]        TEXT UNIQUE,
  [ip]          TEXT UNIQUE,
  [log]         TEXT UNIQUE,
  [email]       TEXT UNIQUE,
  [password]    VARCHAR(64),
  [status]      INTEGER NOT NULL DEFAULT 0,
  [time_login]  DATETIME NOT NULL DEFAULT (datetime('now','localtime')),
  [last_active] DATETIME NOT NULL DEFAULT (datetime('now','localtime'))
);

DROP TABLE IF EXISTS [inquiries];
CREATE TABLE [inquiries] (
  [_id]         TEXT UNIQUE PRIMARY KEY,
  [name] varchar(128) DEFAULT NULL,
  [zusatz] mediumtext
); 

DROP TABLE IF EXISTS [autoresponders];
CREATE TABLE [autoresponders] (
  [_id]         TEXT UNIQUE PRIMARY KEY,
  [name]        VARCHAR(36) UNIQUE NOT NULL,
  [zusatz] mediumtext,
);

COMMIT;